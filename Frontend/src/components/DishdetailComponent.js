import React, { Component } from 'react'
import { CardTitle, Card, CardImg, CardBody, CardText, Breadcrumb, BreadcrumbItem, CardImgOverlay, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody, Label, Row } from 'reactstrap';
import { LocalForm, Control, Errors } from 'react-redux-form';
import Loading from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';

const RenderDish = ({ dish, favorite, postFavorite }) => {
	return (
		<div className="col-12 col-md-5 m-1">
			<Card>
				<CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
				<CardImgOverlay>
					<Button outline color="primary" onClick={() => favorite ? console.log('Already favorite') : postFavorite(dish._id)} style={{backgroundColor: "black"}}>
						{favorite ?
							<span className="fa fa-heart"></span>
							: 
							<span className="fa fa-heart-o"></span>
						}
					</Button>
                </CardImgOverlay>				
				<CardBody>
					<CardTitle className="font-weight-bold">{dish.name}</CardTitle>
					<CardText>{dish.description}</CardText>
				</CardBody>
			</Card>
		</div>
	);
}

const RenderComments = ({ comments, postComment, dishId, commentsErrMess }) => {
	if (comments != null)
		return (
			<div className="col-12 col-md-5 m-1">
				<h4>Comments</h4>
				<ul className="list-unstyled">
					{comments.map((comment) => {

						return (
							<li key={comment._id} className="mt-3">
								<p>{comment.comment}</p>
								<p>{comment.rating} stars</p>
								<p>-- {comment.author.firstname} {comment.author.lastname}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.updatedAt)))}</p>
							</li>

						);
					})}
				</ul>
				<CommentFormcomponent postComment={postComment} dishId={dishId} />
			</div>
		);

	else
		return(
			<div></div>
		);
}

const DishDetail = (props) => {
	if (props.isLoading) {
		return (
			<div className="container">
				<div className="row">
					<Loading />
				</div>
			</div>
		);
	}
	else if (props.errMess) {
		return (
			<div className="container">
				<div className="row">
					<h4>{props.errMess}</h4>
				</div>
			</div>
		)
	}
	else if (props.dish) {
		return (
			<div className="container">
				<div className="row">
					<Breadcrumb>
						<BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
						<BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
					</Breadcrumb>
					<div className="col-12">
						<h3>{props.dish.name}</h3>
						<hr />
					</div>
				</div>
				<div className="row">
					<RenderDish dish={props.dish} favorite={props.favorite} postFavorite={props.postFavorite}/>
					<RenderComments comments={props.comments}
						postComment={props.postComment}
						dishId={props.dish._id}
						commentsErrMess={props.commentsErrMess} />
				</div>
			</div>
		);
	}
	else {
		return (
			<div></div>
		);
	}

}

class CommentFormcomponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isNavOpen: false,
			isModalOpen: false
		};

		this.toggleModal = this.toggleModal.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	toggleModal = () => {
		this.setState({
			isModalOpen: !this.state.isModalOpen
		});
	}

	handleSubmit = (values) => {
		this.toggleModal();
		this.props.postComment(this.props.dishId, values.rating, values.comment);
	}

	render() {
		const required = (val) => val && val.length;
		const minLength = (len) => (val) => !required(val) || val.length >= len;
		const maxLength = (len) => (val) => !required(val) || val.length <= len;

		return (
			<>
				<Button outline onClick={this.toggleModal}>
					<span className="fa fa-edit"></span>{' '}
				   Submit Comment
			   </Button>
				<Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
					<ModalHeader toggle={this.toggleModal}>
						Submit Comment
				   </ModalHeader>
					<ModalBody>
						<LocalForm className="container" onSubmit={this.handleSubmit}>
							<Row className="form-group">
								<Col>
								<Label for="rating">
									Rating
							   </Label>
								<Control.select model=".rating" id="rating" name="rating" className="form-control">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</Control.select>
								</Col>
							</Row>
							<Row className="form-group">
								<Label for="comment">
									Comment
							   </Label>
								<Control.textarea model=".comment" className="form-control" id="comment" name="comment" rows="6" />
							</Row>
							{/* <Row className="form-group"> */}
								<Button color="primary" type="submit">
									Submit
							   </Button>
							{/* </Row> */}
						</LocalForm>
					</ModalBody>
				</Modal>
			</>
		)
	}
}

export default DishDetail;