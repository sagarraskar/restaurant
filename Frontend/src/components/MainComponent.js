import React, { useEffect } from 'react';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import DishDetail from './DishdetailComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Contact from './ContactComponent';
import About from './AboutComponent';
import { connect } from 'react-redux';
import { postComment, fetchDishes, fetchComments, fetchPromos, fetchLeaders, postFeedback, loginUser, logoutUser, signupUser, fetchFavorites, postFavorite, deleteFavorite } from '../redux/ActionCreators'
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import Favorites from './FavoriteComponent';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        leaders: state.leaders,
        promotions: state.promotions,
        favorites: state.favorites,
        auth: state.auth
    };
}

const mapDispathctoProps = (dispatch) => ({
    postComment: (dishId, rating, comment) => dispatch(postComment(dishId, rating, comment)),
    fetchDishes: () => {dispatch(fetchDishes())},
    resetFeedbackForm: () => { dispatch(actions.reset('feedback'))},
    fetchComments : () => {dispatch(fetchComments())},
    fetchPromos: () => {dispatch(fetchPromos())},
    fetchLeaders: () => {dispatch(fetchLeaders())},
    postFeedback: (feedback) => {dispatch(postFeedback(feedback))},
    loginUser: (creds) => dispatch(loginUser(creds)),
    logoutUser: () => dispatch(logoutUser()),
    signupUser: (values) => dispatch(signupUser(values)),
    fetchFavorites: () => {dispatch(fetchFavorites())},
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    deleteFavorite: (dishId) => dispatch(deleteFavorite(dishId))
});


function Main(props) {

    useEffect(()=> {
        props.fetchDishes();
        props.fetchPromos();
        props.fetchComments();
        props.fetchLeaders();
        props.fetchFavorites();
    }, [])
    
    const HomePage = ()=>{
        return(
            <Home 
                dish = {props.dishes.dishes.filter((dish) => dish.featured)[0]}
                dishesLoading = {props.dishes.isLoading}
                dishesErrMess = {props.dishes.errMess}
                promotion = {props.promotions.promotions.filter((promotion)=> promotion.featured)[0]}
                promosLoading = {props.promotions.isLoading}
                promosErrMess = {props.promotions.errMess}
                leader = {props.leaders.leaders.filter((leader) => leader.featured)[0]}
                leadersLoading = {props.leaders.isLoading}
                leadersErrMess = {props.leaders.errMess} 
            />
        )
    }

    const DishWithId = ({match}) => {
        return(
          props.auth.isAuthenticated
          ?
          <DishDetail dish={props.dishes.dishes.filter((dish) => dish._id === match.params.dishId)[0]}
            isLoading={props.dishes.isLoading}
            errMess={props.dishes.errMess}
            comments={props.comments.comments.filter((comment) => comment.dish === match.params.dishId)}
            commentsErrMess={props.comments.errMess}
            postComment={props.postComment}
            favorite={props.favorites.favorites?.dishes.some((dish) => dish._id === match.params.dishId)}
            postFavorite={props.postFavorite}
            />
          :
          <DishDetail dish={props.dishes.dishes.filter((dish) => dish._id === match.params.dishId)[0]}
            isLoading={props.dishes.isLoading}
            errMess={props.dishes.errMess}
            comments={props.comments.comments.filter((comment) => comment.dish === match.params.dishId)}
            commentsErrMess={props.comments.errMess}
            postComment={props.postComment}
            favorite={false}
            postFavorite={props.postFavorite}
            />
        );
      }

    const PrivateRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(prop) => (
          props.auth.isAuthenticated
            ? <Component {...prop} />
            : <Redirect to={{
                pathname: '/home',
                state: { from: prop.location }
              }} />
        )} />
    );
    return (
        <div className="App">
            <Header auth={props.auth}
                loginUser={props.loginUser}
                logoutUser={props.logoutUser}
                signupUser={props.signupUser}
            />
            <TransitionGroup>
                <CSSTransition key={props.location.key} classNames="page" timeout={300}>
                    <Switch>
                        <Route path="/home" component={HomePage}/>
                        <Route exact path="/menu" component={()=> <Menu dishes={props.dishes}/>}/>
                        <Route path="/menu/:dishId" component={DishWithId}/>
                        <PrivateRoute exact path="/favorites" component={()=> <Favorites favorites={props.favorites} deleteFavorite={props.deleteFavorite} />}/>
                        <Route path="/aboutus" component={()=> <About leaders={props.leaders}/>}/>
                        <Route exact path="/contactus" component={() => <Contact resetFeedbackForm={props.resetFeedbackForm} postFeedback={props.postFeedback}/>}/>
                        <Redirect to="/home"/>
                    </Switch>
                </CSSTransition>
            </TransitionGroup>
            <Footer/>
        </div>
    );
}

export default withRouter(connect(mapStateToProps, mapDispathctoProps)(Main));
