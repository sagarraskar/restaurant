const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Favourites = require('../models/favorite');
const router = express.Router();

router.use(bodyParser.json());

router.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favourites.findOne({ user: req.user._id })
            .populate('user')
            .exec((err, favorites) => {
                if (err) return next(err)
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorites);
            });
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favourites.findOne({ user: req.user._id })
            .then((favorite) => {
                if (favorite === null) {
                    Favourites.create({ user: req.user._id, dishes: [] })
                        .then((favorite) => {
                            for (let dish of req.body) {
                                favorite.dishes.push(dish._id);
                            }
                            favorite.save()
                                .then((favorite) => {
                                    Favourites.findById(favorite._id)
                                        .populate('user')
                                        .populate('dishes')
                                        .then((favorite) => {
                                            res.statusCode = 200;
                                            res.setHeader('Content-Type', 'application/json');
                                            res.json(favorite)
                                        }, (err) => next(err))
                                        .catch((err) => next(err));

                                }, (err) => next(err))
                                Loading .catch((err) => next(err));

                        })
                        .catch((err) => next(err));
                }

                else {
                    for (let dish of req.body) {
                        if (favorite.dishes.indexOf(dish._id) < 0) {
                            favorite.dishes.push(dish)
                        }
                    }
                    favorite.save()
                        .then((favorite) => {
                            Favourites.findById(favorite._id)
                                .populate('user')
                                .populate('dishes')
                                .then((favorite) => {
                                    res.statusCode = 200;
                                    res.setHeader('Content-Type', 'application/json');
                                    res.json(favorite)
                                }, (err) => next(err))
                                .catch((err) => next(err));

                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));

    })
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /favorites');
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favourites.findOne({ user: req.user._id }).remove()
            .then((favorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
    })

router.route('/:dishId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
       Favourites.findOne({user: req.user._id})
       .then((favorites) => {
            if (!favorites) {
                res.statusCode = 200;
                res.setHeader('content-type', 'application/json');
                return res.json({"exists": false, "favorites": favorites});
            }
            else {
                if(favorites.dishes.indexOf(req.params.dishId) < 0){
                    res.statusCode = 200;
                    res.setHeader('content-type', 'application/json');
                    return res.json({"exists": false, "favorites": favorites});
                }
                else {
                    res.statusCode = 200;
                    res.setHeader('content-type', 'application/json');
                    return res.json({"exists": true, "favorites": favorites});
                }
            }
       }, (err) => next(err))
       .catch((err) => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favourites.findOne({ user: req.user._id })
            .then((favorite) => {
                if (!favorite) {
                    Favourites.create({ user: req.user._id, dishes: [] })
                    .then(favorite => {
                        favorite.dishes.push({"_id": req.params.dishId});
                        favorite.save()
                        .then((favorite) => {
                            Favorites.findById(favorite._id)
                            .populate('user')
                            .populate('dishes')
                            .then((favorite) => {
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorite);
                            })
                        })
                        .catch((err) => {
                            return next(err);
                        });
                    })
                    .catch((err) => {
                        return next(err)
                    });
                }
                else {
                    if (favorite.dishes.indexOf(req.params.dishId) < 0) {
                        favorite.dishes.push(req.body);
                    
                        favorite.save()
                        .then((favorite) => {
                            Favourites.findById(favorite._id)
                                .populate('user')
                                .populate('dishes')
                                .then((favorite) => {
                                    res.statusCode = 200;
                                    res.setHeader('Content-Type', 'application/json');
                                    res.json(favorite);
                                }, (err) => next(err))
                                .catch((err) => next(err));
                        }, (err) => next(err))
                        .catch((err) => next(err));
                    }
                }
            }, (err) => next(err))
            .catch(err => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /favorites/' + req.params.dishId);
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favourites.findOne({ user: req.user._id })
            .then((favorite) => {
                if (favorite === null) {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({"n": 0, "ok": 1, "deletedCount": 0});
                }
                else {
                    let index = favorite.dishes.indexOf(req.params.dishId);
                    if (index !== -1) {
                        favorite.dishes.splice(index, 1);
                    }
                    favorite.save()
                        .then((favorite) => {
                            Favourites.findById(favorite._id)
                                .populate('user')
                                .populate('dishes')
                                .then((favorite) => {
                                    res.statusCode = 200;
                                    res.setHeader('Content-Type', 'application/json');
                                    res.json(favorite);
                                }, (err) => next(err))
                                .catch(err => next(err));
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch(err => next(err));

    });

module.exports = router;

