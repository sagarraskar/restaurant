const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const Feedback = require('../models/feedback');
const cors = require('./cors');

const feedbackRouter = express.Router();

feedbackRouter.use(bodyParser.json());

feedbackRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser,  authenticate.verifyAdmin, (req, res, next) => {
    Feedback.find()
    .then((feedbacks) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'appliatino/json');
        res.json(feedbacks);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, (req, res, next) => {
    if(req.body != null){
        Feedback.create(req.body)
        .then(feedback => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(feedback);
        }, (err) => next(err))
        .catch((err) => next(err));
    }
    else{
        const err = new Error("Feedback not found in request body\n");
        err.status = 404;
        return next(err);

    }
})

module.exports = feedbackRouter;