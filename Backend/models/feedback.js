const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feedbackSchema = new Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type:String,
        required: true
    },
    telnum: {
        type: Number
    },
    email: {
        type: String
    },
    agree: {
        type: Boolean
    },
    contactType: {
        type: String
    },
    message: {
        type: String
    }
}, {
    timestamps: true
});

var Feedback = mongoose.model('Feedback', feedbackSchema);
module.exports = Feedback;